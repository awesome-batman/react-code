const Table = (
    {
        columns = [],
        data = []
    }
) => (
    <div className="page-content page-content-table">
        <table className="table is-indent">
            <thead>
            <tr>
                {
                    columns.map(({title, width}) => (
                        <th scope="col" className={`text-center col-${width}`}>
                            {title}
                        </th>
                    ))
                }
            </tr>
            </thead>
            <tbody>
            {
                data.map(item => (
                    <tr className="odd">
                        {
                            columns.map(({render, width}) => (
                                <td className={`text-center col-${width}`}>
                                    {render(item)}
                                </td>
                            ))
                        }
                    </tr>
                ))
            }
            </tbody>
        </table>
    </div>
);

export default Table;