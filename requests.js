import contactRelationsFactory from "../../../../factories/contact-relations-factory";

export default class Requests {

    constructor(component) {
        this.component = component;
        this.factory = contactRelationsFactory(`/v2/contacts/${component.props.contact_id}/relations`);
    }

    load() {
        const {component} = this,
            {limit} = component.state;

        this.factory.request('get', {limit})
            .then(({contacts, total}) => {
                component.setState({contacts, total}, () => component.events.showBlock('.contact-collapse'));
                component.events.toggleLoadingStatus(false);
            })
            .catch(component.handleError);
    }

    save() {
        const {component} = this,
            {relationship, notes} = component.state.form,
            relation_id = component.state.form.contact.id;

        this.factory.request('post', {relationship, notes, relation_id})
            .then(({message}) => {
                this.load();
                component.events.toggleForm();
                component.events.toggleLoadingStatus(false);
                toastr.success(message);
            })
            .catch(data => {
                    component.handleError(data);
                    component.events.toggleLoadingStatus(false);
                }
            );
    }

    edit() {
        const {component} = this,
            {contact, relationship, notes} = component.state.form,
            {contact_id, relation_id} = contact.pivot;

        this.factory.request('put', {contact_id, relation_id, relationship, notes})
            .then(({message}) => {
                component.events.hideAllForms();
                component.events.hideCardLoading();
                this.load();
                toastr.success(message);
            })
            .catch(data => {
                component.events.hideAllForms();
                component.events.hideCardLoading();
                component.handleError(data);
            });
    }

    destroy(relation_id) {
        const {component} = this;
        const {contact_id} = component.props;
        this.factory.request('delete', {contact_id, relation_id})
            .then(({message}) => {
                component.el.selector.find(`.contact-relation-${relation_id}`).hide(500);
                this.load();
                toastr.success(message);
            })
            .catch(data => {
                component.events.hideCardLoading();
                component.handleError(data);
            });
    }
}