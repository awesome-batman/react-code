import Table from "../utilities/table/component";
import Button from "../utilities/button/component";
import ChecklistModal from "./new-checklist-modal/component";

export default class {
    default(component) {
        const {
            props: {
                recordType,
                propertyId,
                entityId,
                address,
                user
            },
            state: {
                checklists
            },
            events: {
                showModal,
                deleteChecklist,
                editChecklist,
                onChecklistCreated
            },
            newChecklistModalId,
        } = component;
        return (
            <div className="white-container">
                <div className="row">
                    <div className="col-4">
                        <Button onClick={showModal} org>
                            + New Checklist
                        </Button>
                    </div>
                </div>
                <hr/>
                <Table data={checklists}
                       columns={[
                           {
                               title: 'Name',
                               render: ({name}) => <p>{name}</p>
                           },
                           {
                               title: 'Created',
                               render: ({created_at}) =>
                                   <p>{moment(created_at, 'YYYY-MM-DD H:i:s').format('DD/MM/YYYY')}</p>
                           },
                           {
                               title: 'Updated',
                               render: ({updated_at}) =>
                                   <p>{moment(updated_at, 'YYYY-MM-DD H:i:s').format('DD/MM/YYYY')}</p>
                           },
                           {
                               title: 'Completion',
                               render: this.getCompletion
                           },
                           {
                               title: 'Actions',
                               render: ({id}) => (
                                   <div>
                                       <Button type="warning"
                                               addClass="btn-org-s mr-2"
                                               onClick={() => editChecklist(id)}>
                                           <i className="fa fa-pencil"/>
                                       </Button>
                                       <Button type="danger"
                                               addClass="btn-org-s"
                                               onClick={() => deleteChecklist(id)}>
                                           <i className="fa fa-trash"/>
                                       </Button>
                                   </div>
                               )
                           }
                       ]}/>
                <ChecklistModal modalId={newChecklistModalId}
                                recordType={recordType}
                                propertyId={propertyId}
                                entityId={entityId}
                                address={address}
                                user={user}
                                onSave={onChecklistCreated}/>

            </div>
        );
    }

    getCompletion({checklist_items}) {
        const total = checklist_items.length;
        const completed = checklist_items.filter(({status}) => status === Laravel.ChecklistItem.COMPLETED_YES).length;

        return <p>{completed ? Math.round(completed * 100 / total) : 0} %</p>;
    }
}