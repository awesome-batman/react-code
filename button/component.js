const Button = (
    {
        type = 'info',
        addClass = '',
        org = false,
        block = false,
        loading = false,
        disabled = false,
        data = {},
        style = {},
        onClick = () => {},
        children
    }
) => (
    <button className={`btn btn-${type} ${org && 'btn-org'} ${block && 'btn-block'} ${addClass}`}
            onClick={onClick}
            style={style}
            disabled={disabled}
            {...data}>
        {children}
        <span className={`fa fa-spinner ml-5 fa-spin ${loading ? ' ' : 'd-none'}`}/>
    </button>
);

export default Button;